<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * $Id: S3.php 47 2009-07-20 01:25:40Z don.schonknecht $
 *
 * Copyright (c) 2008, Donovan Schönknecht.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Amazon S3 is a trademark of Amazon.com, Inc. or its affiliates.
 */

/**
 * Amazon S3 PHP class
 *
 * @link http://undesigned.org.za/2007/10/22/amazon-s3-php-class
 * @version 0.4.0
 */
class S3 {

	// ACL flags
	const ACL_PRIVATE = 'private';
	const ACL_PUBLIC_READ = 'public-read';
	const ACL_PUBLIC_READ_WRITE = 'public-read-write';
	const ACL_AUTHENTICATED_READ = 'authenticated-read';

	// storage class flags
	const STORAGE_CLASS_STANDARD = 'STANDARD';
	const STORAGE_CLASS_RRS = 'REDUCED_REDUNDANCY';

	// server-side encryption flags
	const SSE_NONE = '';
	const SSE_AES256 = 'AES256';

	// authorization mechanism
	const AUTHUORIZATION_MECHANISM = 'AWS4-HMAC-SHA256';

	// request flags
	const AWS_REQUEST_SERVICE = 's3';
	const AWS_REQUEST_SIGNITURE = 'aws4_request';

	public static $use_ssl = false;
	public static $verify_peer = true;

	private static $__access_key = NULL;
	private static $__secret_key = NULL;
	private static $__region = NULL;

	function __construct($config = array())
	{
		if ( ! empty($config))
		{
			$this->initialize($config);
		}

		log_message('debug', 'S3 Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * Initialize preferences
	 *
	 * @access	public
	 * @param	array
	 * @return	void
	 */
	function initialize($config = array())
	{
		extract($config);

		if ( ! empty($access_key) AND ! empty($secret_key) AND ! empty($region))
		{
			self::setAuth($access_key, $secret_key, $region);
		}

		self::$use_ssl = $use_ssl;
		self::$verify_peer = $verify_peer;
	}

	/**
	 * Set AWS access key and secret key
	 *
	 * @param string $accessKey Access key
	 * @param string $secretKey Secret key
	 * @return void
	 */
	public static function setAuth($accessKey, $secretKey, $region)
	{
		self::$__access_key = $accessKey;
		self::$__secret_key = $secretKey;
		self::$__region = $region;
	}

	/**
	 * Get a list of buckets
	 *
	 * @param boolean $detailed Returns detailed bucket list when true
	 * @return array | false
	 */
	public static function listBuckets($detailed = false)
	{
		$rest = new S3Request('GET', '', '');
		$rest = $rest->getResponse();
		if ($rest->error === false && $rest->code !== 200)
			$rest->error = array('code' => $rest->code, 'message' => 'Unexpected HTTP status');
		if ($rest->error !== false)
		{
			trigger_error(sprintf("S3::listBuckets(): [%s] %s", $rest->error['code'], $rest->error['message']), E_USER_WARNING);
			return false;
		}
		$results = array();
		if (!isset($rest->body->Buckets))
			return $results;

		if ($detailed)
		{
			if (isset($rest->body->Owner, $rest->body->Owner->ID, $rest->body->Owner->DisplayName))
				$results['owner'] = array(
					'id' => (string) $rest->body->Owner->ID, 'name' => (string) $rest->body->Owner->ID
				);
			$results['buckets'] = array();
			foreach ($rest->body->Buckets->Bucket as $b)
				$results['buckets'][] = array(
					'name' => (string) $b->Name, 'time' => strtotime((string) $b->CreationDate)
				);
		} else
			foreach ($rest->body->Buckets->Bucket as $b)
				$results[] = (string) $b->Name;

		return $results;
	}

	/*
	 * Get contents for a bucket
	 *
	 * If maxKeys is null this method will loop through truncated result sets
	 *
	 * @param string $bucket Bucket name
	 * @param string $prefix Prefix
	 * @param string $marker Marker (last file listed)
	 * @param string $maxKeys Max keys (maximum number of keys to return)
	 * @param string $delimiter Delimiter
	 * @param boolean $returnCommonPrefixes Set to true to return CommonPrefixes
	 * @return array | false
	 */

	public static function getBucket($bucket, $prefix = null, $marker = null, $maxKeys = null, $delimiter = null, $returnCommonPrefixes = false)
	{
		$rest = new S3Request('GET', $bucket, '');
		if ($prefix !== null && $prefix !== '')
			$rest->setParameter('prefix', $prefix);
		if ($marker !== null && $marker !== '')
			$rest->setParameter('marker', $marker);
		if ($maxKeys !== null && $maxKeys !== '')
			$rest->setParameter('max-keys', $maxKeys);
		if ($delimiter !== null && $delimiter !== '')
			$rest->setParameter('delimiter', $delimiter);
		$rest->setAmzHeader('x-amz-content-sha256', 'UNSIGNED-PAYLOAD');
		$response = $rest->getResponse("GET");
		if ($response->error === false && $response->code !== 200)
			$response->error = array('code' => $response->code, 'message' => 'Unexpected HTTP status');
		if ($response->error !== false)
		{
			trigger_error(sprintf("S3::getBucket(): [%s] %s", $response->error['code'], $response->error['message']), E_USER_WARNING);
			return false;
		}

		$results = array();

		$nextMarker = null;
		if (isset($response->body, $response->body->Contents))
			foreach ($response->body->Contents as $c)
			{
				$results[(string) $c->Key] = array(
					'name' => (string) $c->Key,
					'time' => strtotime((string) $c->LastModified),
					'size' => (int) $c->Size,
					'hash' => substr((string) $c->ETag, 1, -1)
				);
				$nextMarker = (string) $c->Key;
			}

		if ($returnCommonPrefixes && isset($response->body, $response->body->CommonPrefixes))
			foreach ($response->body->CommonPrefixes as $c)
				$results[(string) $c->Prefix] = array('prefix' => (string) $c->Prefix);

		if (isset($response->body, $response->body->IsTruncated) &&
				(string) $response->body->IsTruncated == 'false')
			return $results;

		if (isset($response->body, $response->body->NextMarker))
			$nextMarker = (string) $response->body->NextMarker;

		// Loop through truncated results if maxKeys isn't specified
		if ($maxKeys == null && $nextMarker !== null && (string) $response->body->IsTruncated == 'true')
			do
			{
				$rest = new S3Request('GET', $bucket, '');
				if ($prefix !== null && $prefix !== '')
					$rest->setParameter('prefix', $prefix);
				$rest->setParameter('marker', $nextMarker);
				if ($delimiter !== null && $delimiter !== '')
					$rest->setParameter('delimiter', $delimiter);

				if (($response = $rest->getResponse(true)) == false || $response->code !== 200)
					break;

				if (isset($response->body, $response->body->Contents))
					foreach ($response->body->Contents as $c)
					{
						$results[(string) $c->Key] = array(
							'name' => (string) $c->Key,
							'time' => strtotime((string) $c->LastModified),
							'size' => (int) $c->Size,
							'hash' => substr((string) $c->ETag, 1, -1)
						);
						$nextMarker = (string) $c->Key;
					}

				if ($returnCommonPrefixes && isset($response->body, $response->body->CommonPrefixes))
					foreach ($response->body->CommonPrefixes as $c)
						$results[(string) $c->Prefix] = array('prefix' => (string) $c->Prefix);

				if (isset($response->body, $response->body->NextMarker))
					$nextMarker = (string) $response->body->NextMarker;
			} while ($response !== false && (string) $response->body->IsTruncated == 'true');

		return $results;
	}

	/**
	 * Put a bucket
	 *
	 * @param string $bucket Bucket name
	 * @param constant $acl ACL flag
	 * @param string $location Set as "EU" to create buckets hosted in Europe
	 * @return boolean
	 */
	public static function putBucket($bucket, $acl = self::ACL_PRIVATE, $location = false)
	{
		$rest = new S3Request('PUT', $bucket, '');
		$rest->setAmzHeader('x-amz-acl', $acl);

		if ($location !== false)
		{
			$dom = new DOMDocument;
			$createBucketConfiguration = $dom->createElement('CreateBucketConfiguration');
			$locationConstraint = $dom->createElement('LocationConstraint', strtolower($location));
			$createBucketConfiguration->appendChild($locationConstraint);
			$dom->appendChild($createBucketConfiguration);
			$rest->data = $dom->saveXML();
			$rest->size = strlen($rest->data);
			$rest->setHeader('Content-Type', 'application/xml');
		}
		$rest = $rest->getResponse();

		if ($rest->error === false && $rest->code !== 200)
			$rest->error = array('code' => $rest->code, 'message' => 'Unexpected HTTP status');
		if ($rest->error !== false)
		{
			trigger_error(sprintf("S3::putBucket({$bucket}, {$acl}, {$location}): [%s] %s",
							$rest->error['code'], $rest->error['message']), E_USER_WARNING);
			return false;
		}
		return true;
	}

	/**
	 * Delete an empty bucket
	 *
	 * @param string $bucket Bucket name
	 * @return boolean
	 */
	public static function deleteBucket($bucket)
	{
		$rest = new S3Request('DELETE', $bucket);
		$rest = $rest->getResponse();
		if ($rest->error === false && $rest->code !== 204)
			$rest->error = array('code' => $rest->code, 'message' => 'Unexpected HTTP status');
		if ($rest->error !== false)
		{
			trigger_error(sprintf("S3::deleteBucket({$bucket}): [%s] %s",
							$rest->error['code'], $rest->error['message']), E_USER_WARNING);
			return false;
		}
		return true;
	}

	/**
	 * Get an object
	 *
	 * @param string $bucket Bucket name
	 * @param string $uri Object URI
	 * @param mixed $saveTo Filename or resource to write to
	 * @return mixed
	 */
	public static function getObject($bucket, $uri, $saveTo = false)
	{
		$rest = new S3Request('GET', $bucket, $uri);
		if ($saveTo !== false)
		{
			if (is_resource($saveTo))
				$rest->fp = & $saveTo;
			else
			if (($rest->fp = @fopen($saveTo, 'wb')) !== false)
				$rest->file = realpath($saveTo);
			else
				$rest->response->error = array('code' => 0, 'message' => 'Unable to open save file for writing: ' . $saveTo);
		}
		if ($rest->response->error === false)
			$rest->setAmzHeader('x-amz-content-sha256', 'UNSIGNED-PAYLOAD');
			$rest->getResponse("GET");

		if ($rest->response->error === false && $rest->response->code !== 200)
			$rest->response->error = array('code' => $rest->response->code, 'message' => 'Unexpected HTTP status');
		if ($rest->response->error !== false)
		{
			trigger_error(sprintf("S3::getObject({$bucket}, {$uri}): [%s] %s",
							$rest->response->error['code'], $rest->response->error['message']), E_USER_WARNING);
			return false;
		}
		return $rest->response;
	}

	/**
	 * Put an object from a file (legacy function)
	 *
	 * @param string $file Input file path
	 * @param string $bucket Bucket name
	 * @param string $uri Object URI
	 * @param constant $acl ACL constant
	 * @param array $metaHeaders Array of x-amz-meta-* headers
	 * @param string $contentType Content type
	 * @return boolean
	 */
	public static function putObjectFile($file, $bucket, $uri, $acl = self::ACL_PUBLIC_READ, $metaHeaders = array(), $contentType = null)
	{
		return self::__upload(self::__getFileData($file), $bucket, $uri, $acl, $metaHeaders, $contentType);
	}

	/**
	 * Put an object from a string (legacy function)
	 *
	 * @param string $string Input data
	 * @param string $bucket Bucket name
	 * @param string $uri Object URI
	 * @param constant $acl ACL constant
	 * @param array $metaHeaders Array of x-amz-meta-* headers
	 * @param string $contentType Content type
	 * @return boolean
	 */
	public static function putObjectString($string, $bucket, $uri, $acl = self::ACL_PUBLIC_READ, $metaHeaders = array(), $contentType = 'text/plain')
	{
		return self::__upload($string, $bucket, $uri, $acl, $metaHeaders, $contentType);
	}

	/**
	 * Copy an object
	 *
	 * @param string $bucket Source bucket name
	 * @param string $uri Source object URI
	 * @param string $bucket Destination bucket name
	 * @param string $uri Destination object URI
	 * @param constant $acl ACL constant
	 * @param array $metaHeaders Optional array of x-amz-meta-* headers
	 * @param array $requestHeaders Optional array of request headers (content type, disposition, etc.)
	 * @param constant $storageClass Storage class constant
	 * @return mixed | false
	 */
	public static function copyObject($srcBucket, $srcUri, $bucket, $uri, $acl = self::ACL_PRIVATE, $metaHeaders = array(), $requestHeaders = array(), $storageClass = self::STORAGE_CLASS_STANDARD)
	{
		$rest = new S3Request('PUT', $bucket, $uri);
		$rest->setHeader('Content-Length', 0);
		foreach ($requestHeaders as $h => $v)
			$rest->setHeader($h, $v);
		foreach ($metaHeaders as $h => $v)
			$rest->setAmzHeader('x-amz-meta-' . $h, $v);
		if ($storageClass !== self::STORAGE_CLASS_STANDARD) // Storage class
			$rest->setAmzHeader('x-amz-storage-class', $storageClass);
		$rest->setAmzHeader('x-amz-acl', $acl);
		$rest->setAmzHeader('x-amz-content-sha256', 'UNSIGNED-PAYLOAD');
		$rest->setAmzHeader('x-amz-copy-source', sprintf('/%s/%s', $srcBucket, $srcUri));
		if (sizeof($requestHeaders) > 0 || sizeof($metaHeaders) > 0)
			$rest->setAmzHeader('x-amz-metadata-directive', 'REPLACE');
		$rest = $rest->getResponse("PUT");
		if ($rest->error === false && $rest->code !== 200)
			$rest->error = array('code' => $rest->code, 'message' => 'Unexpected HTTP status');
		if ($rest->error !== false)
		{
			trigger_error(sprintf("S3::copyObject({$srcBucket}, {$srcUri}, {$bucket}, {$uri}): [%s] %s",
							$rest->error['code'], $rest->error['message']), E_USER_WARNING);
			return false;
		}
		return isset($rest->body->LastModified, $rest->body->ETag) ? array(
			'time' => strtotime((string) $rest->body->LastModified),
			'hash' => substr((string) $rest->body->ETag, 1, -1)
				) : false;
	}

	/**
	 * Delete an object
	 *
	 * @param string $bucket Bucket name
	 * @param string $uri Object URI
	 * @return boolean
	 */
	public static function deleteObject($bucket, $uri)
	{
		$rest = new S3Request('DELETE', $bucket, $uri);
		$rest->setAmzHeader('x-amz-content-sha256', 'UNSIGNED-PAYLOAD');
		$rest = $rest->getResponse("DELETE");
		if ($rest->error === false && $rest->code !== 204)
			$rest->error = array('code' => $rest->code, 'message' => 'Unexpected HTTP status');
		if ($rest->error !== false)
		{
			trigger_error(sprintf("S3::deleteObject(): [%s] %s",
							$rest->error['code'], $rest->error['message']), E_USER_WARNING);
			return false;
		}
		return true;
	}

	/**
	 * Put an object
	 *
	 * @param mixed $input Input data
	 * @param string $bucket Bucket name
	 * @param string $uri Object URI
	 * @param constant $acl ACL constant
	 * @param array $metaHeaders Array of x-amz-meta-* headers
	 * @param array $requestHeaders Array of request headers or content type as a string
	 * @param constant $storageClass Storage class constant
	 * @param constant $serverSideEncryption Server-side encryption
	 * @return boolean
	 */
	public static function __upload($input, $bucket, $uri, $acl = self::ACL_PUBLIC_READ, $metaHeaders = array(), $requestHeaders = array(), $storageClass = self::STORAGE_CLASS_STANDARD, $serverSideEncryption = self::SSE_NONE)
	{
		if ($input === false)
			return false;
		$rest = new S3Request('PUT', $bucket, $uri);

		if (is_string($input))
			$input = array(
				'data' => $input,
				'size' => strlen($input)
			);

		// Data
		$rest->data = $input['data'];

		// Content-Length (required)
		$rest->size = $input['size'];

		// Custom request headers (Content-Type, Content-Disposition, Content-Encoding)
		if (is_array($requestHeaders))
			foreach ($requestHeaders as $h => $v)
				$rest->setHeader($h, $v);

		// Content-Type
		if (!isset($input['type']))
		{
			if (isset($requestHeaders['Content-Type']))
				$input['type'] = & $requestHeaders['Content-Type'];
			elseif (isset($input['path']))
				$input['type'] = self::__getMimeType($input['path']);
			else
				$input['type'] = 'application/octet-stream';
		}

		// Storage class
		if ($storageClass !== self::STORAGE_CLASS_STANDARD)
			$rest->setAmzHeader('x-amz-storage-class', $storageClass);

		// Server-side encryption
		if ($serverSideEncryption !== self::SSE_NONE)
			$rest->setAmzHeader('x-amz-server-side-encryption', $serverSideEncryption);

		// We need to post with Content-Length and Content-Type, MD5 is optional
		if ($rest->size >= 0 && $rest->data !== false)
		{
			$rest->setHeader('Content-Type', $input['type']);
			$rest->setAmzHeader('x-amz-content-sha256', hash('sha256', $rest->data));
			$rest->setAmzHeader('x-amz-acl', $acl);
			foreach ($metaHeaders as $h => $v)
				$rest->setAmzHeader('x-amz-meta-' . $h, $v);
			$rest->getResponse("PUT");
		} else
			$rest->response->error = array('code' => 0, 'message' => 'Missing input parameters');

		if ($rest->response->error === false && $rest->response->code !== 200)
			$rest->response->error = array('code' => $rest->response->code, 'message' => 'Unexpected HTTP status');
		if ($rest->response->error !== false)
		{
			trigger_error(sprintf("S3::__upload(): [%s] %s", $rest->response->error['code'], $rest->response->error['message']), E_USER_WARNING);
			return false;
		}
		return true;
	}

	/**
	 * Create input info array
	 *
	 * @param string $path file path
	 * @return array | false
	 */
	public static function __getFileData($path)
	{
		if (!file_exists($path) || !is_file($path) || !is_readable($path))
		{
			trigger_error('S3::__getFileData(): Unable to open input file: ' . $path, E_USER_WARNING);
			return false;
		}
		return array(
			'path' => $path,
			'data' => file_get_contents($path),
			'size' => filesize($path)
		);
	}

	/**
	 * Get MIME type for file
	 *
	 * @internal Used to get mime types
	 * @param string &$file File path
	 * @return string
	 */
	public static function __getMimeType(&$file)
	{
		$type = false;
		// Fileinfo documentation says fileinfo_open() will use the
		// MAGIC env var for the magic file
		if (extension_loaded('fileinfo') && isset($_ENV['MAGIC']) &&
				($finfo = finfo_open(FILEINFO_MIME, $_ENV['MAGIC'])) !== false)
		{
			if (($type = finfo_file($finfo, $file)) !== false)
			{
				// Remove the charset and grab the last content-type
				$type = explode(' ', str_replace('; charset=', ';charset=', $type));
				$type = array_pop($type);
				$type = explode(';', $type);
				$type = trim(array_shift($type));
			}
			finfo_close($finfo);

			// If anyone is still using mime_content_type()
		}
		elseif (function_exists('mime_content_type'))
			$type = trim(mime_content_type($file));

		if ($type !== false && strlen($type) > 0)
			return $type;

		// Otherwise do it the old fashioned way
		static $exts = array(
			'jpg' => 'image/jpeg', 'gif' => 'image/gif', 'png' => 'image/png',
			'tif' => 'image/tiff', 'tiff' => 'image/tiff', 'ico' => 'image/x-icon',
			'swf' => 'application/x-shockwave-flash', 'pdf' => 'application/pdf',
			'zip' => 'application/zip', 'gz' => 'application/x-gzip',
			'tar' => 'application/x-tar', 'bz' => 'application/x-bzip',
			'bz2' => 'application/x-bzip2', 'txt' => 'text/plain',
			'asc' => 'text/plain', 'htm' => 'text/html', 'html' => 'text/html',
			'css' => 'text/css', 'js' => 'text/javascript',
			'xml' => 'text/xml', 'xsl' => 'application/xsl+xml',
			'ogg' => 'application/ogg', 'mp3' => 'audio/mpeg', 'wav' => 'audio/x-wav',
			'avi' => 'video/x-msvideo', 'mpg' => 'video/mpeg', 'mpeg' => 'video/mpeg',
			'mov' => 'video/quicktime', 'flv' => 'video/x-flv', 'php' => 'text/x-php'
		);
		$ext = strtolower(pathInfo($file, PATHINFO_EXTENSION));
		return isset($exts[$ext]) ? $exts[$ext] : 'application/octet-stream';
	}

	/**
	 * Generate the auth string: "AWS AccessKey:Signature"
	 *
	 * @internal Used by S3Request::getResponse()
	 * @param string $string String to sign or uri
	 * @param array $headers header
	 * @return string
	 */
	public static function __getSignature($string, $headers = array(), $method = "PUT")
	{
		$result = "";
		if ($headers)
		{
			// Canonical headers
			$canonical_headers = [];
			foreach($headers as $key => $value) {
				$canonical_headers[] = strtolower($key) . ":" . $value;
			}
			$canonical_headers = implode("\n", $canonical_headers);

			// Signed headers
			$signed_headers = [];
			foreach($headers as $key => $value) {
				$signed_headers[] = strtolower($key);
			}
			$signed_headers = implode(";", $signed_headers);

			// Cannonical request
			$canonical_request = hash('sha256', implode("\n", array(
				$method,
				$string,
				"",
				$canonical_headers,
				"",
				$signed_headers,
				$headers['x-amz-content-sha256']
			)));

			// Date
			$date = gmdate('Ymd');

			// AWS Scope
			$scope = implode('/', [
				$date,
				self::$__region,
				self::AWS_REQUEST_SERVICE,
				self::AWS_REQUEST_SIGNITURE
			]);

			// String to sign
			$string_to_sign = implode("\n", [
				self::AUTHUORIZATION_MECHANISM,
				$headers['Date'],
				$scope,
				$canonical_request
			]);

			// Signing key
			$kSecret = 'AWS4' . self::$__secret_key;
			$kDate = hash_hmac('sha256', $date, $kSecret, true);
			$kRegion = hash_hmac('sha256', self::$__region, $kDate, true);
			$kService = hash_hmac('sha256', self::AWS_REQUEST_SERVICE, $kRegion, true);
			$kSigning = hash_hmac('sha256', self::AWS_REQUEST_SIGNITURE, $kService, true);

			// Signature
			$result = self::AUTHUORIZATION_MECHANISM . ' ' . implode( ',', [
				'Credential=' . self::$__access_key . '/' . $scope,
				'SignedHeaders=' . $signed_headers,
				'Signature=' . hash_hmac('sha256', $string_to_sign, $kSigning)
			]);
		}
		else
		{
			$result = 'AWS ' . self::$__access_key . ':' . self::__getHash($string);
		}

		return $result;
	}

	/**
	 * Creates a HMAC-SHA1 hash
	 *
	 * This uses the hash extension if loaded
	 *
	 * @internal Used by __getSignature()
	 * @param string $string String to sign
	 * @return string
	 */
	private static function __getHash($string)
	{
		return base64_encode(extension_loaded('hash') ?
						hash_hmac('sha1', $string, self::$__secret_key, true) : pack('H*', sha1(
										(str_pad(self::$__secret_key, 64, chr(0x00)) ^ (str_repeat(chr(0x5c), 64))) .
										pack('H*', sha1((str_pad(self::$__secret_key, 64, chr(0x00)) ^
														(str_repeat(chr(0x36), 64))) . $string)))));
	}
}

final class S3Request {

	private $verb, $bucket, $uri, $resource = '', $parameters = array(),
	$amzHeaders = array(), $headers = array(
		'Host' => '', 'Date' => '', 'Content-MD5' => '', 'Content-Type' => ''
	);
	public $fp = false, $size = 0, $data = false, $response;

	/**
	 * Constructor
	 *
	 * @param string $verb Verb
	 * @param string $bucket Bucket name
	 * @param string $uri Object URI
	 * @return mixed
	 */
	function __construct($verb, $bucket = '', $uri = '', $defaultHost = 's3.amazonaws.com')
	{
		$this->verb = $verb;
		$this->bucket = strtolower($bucket);
		$this->uri = $uri !== '' ? '/' . str_replace('%2F', '/', rawurlencode($uri)) : '/';

		if ($this->bucket !== '')
		{
			$this->headers['Host'] = $this->bucket . '.' . $defaultHost;
			$this->resource = '/' . $this->bucket . $this->uri;
		}
		else
		{
			$this->headers['Host'] = $defaultHost;
			//$this->resource = strlen($this->uri) > 1 ? '/'.$this->bucket.$this->uri : $this->uri;
			$this->resource = $this->uri;
		}
		$this->headers['Date'] = gmdate('Ymd\THis\Z');

		$this->response = new STDClass;
		$this->response->error = false;
	}

	/**
	 * Set request parameter
	 *
	 * @param string $key Key
	 * @param string $value Value
	 * @return void
	 */
	public function setParameter($key, $value)
	{
		$this->parameters[$key] = $value;
	}

	/**
	 * Set request header
	 *
	 * @param string $key Key
	 * @param string $value Value
	 * @return void
	 */
	public function setHeader($key, $value)
	{
		$this->headers[$key] = $value;
	}

	/**
	 * Set x-amz-meta-* header
	 *
	 * @param string $key Key
	 * @param string $value Value
	 * @return void
	 */
	public function setAmzHeader($key, $value)
	{
		$this->amzHeaders[$key] = $value;
	}

	/**
	 * Get the S3 response
	 *
	 * @param string $method request method
	 * @return object | false
	 */
	public function getResponse($method = "")
	{
		$query = '';
		if (sizeof($this->parameters) > 0)
		{
			$query = substr($this->uri, -1) !== '?' ? '?' : '&';
			foreach ($this->parameters as $var => $value)
				if ($value == null || $value == '')
					$query .= $var . '&';
				// Parameters should be encoded (thanks Sean O'Dea)
				else
					$query .= $var . '=' . rawurlencode($value) . '&';
			$query = substr($query, 0, -1);
			$this->uri .= $query;

			if (array_key_exists('acl', $this->parameters) ||
					array_key_exists('location', $this->parameters) ||
					array_key_exists('torrent', $this->parameters) ||
					array_key_exists('logging', $this->parameters))
				$this->resource .= $query;
		}
		$url = ((S3::$use_ssl && extension_loaded('openssl')) ?
						'https://' : 'http://') . $this->headers['Host'] . $this->uri;
		//var_dump($this->bucket, $this->uri, $this->resource, $url);
		// Basic setup
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_USERAGENT, 'S3/php');

		if (S3::$use_ssl)
		{
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
			if (S3::$verify_peer)
			{
			    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
			}
			else
			{
			    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			}
		}

		curl_setopt($curl, CURLOPT_URL, $url);

		// Headers
		$headers = array();
		$amz = array();
		foreach ($this->amzHeaders as $header => $value)
		{
			if (strlen($value) > 0)
			{
				if ($method) $signature_headers[$header] = $value;
				else $headers[] = $header . ': ' . $value;
			}
		}

		foreach ($this->headers as $header => $value)
		{
			if (strlen($value) > 0)
			{
				if ($method) $signature_headers[$header] = $value;
				else $headers[] = $header . ': ' . $value;
			}
		}

		// Collect AMZ headers for signature
		foreach ($this->amzHeaders as $header => $value)
			if (strlen($value) > 0)
				$amz[] = strtolower($header) . ':' . $value;

		// AMZ headers must be sorted
		if (sizeof($amz) > 0)
		{
			sort($amz);
			$amz = "\n" . implode("\n", $amz);
		} else
			$amz = '';

		// Sort it in ascending order
		if ($method)
			ksort($signature_headers);

		// Authorization string (CloudFront stringToSign should only contain a date)
		if ($method)
		{
			$headers = ['Authorization: ' . S3::__getSignature($this->uri, $signature_headers, $method)];
			foreach($signature_headers as $key => $value) {
				$headers[] = $key . ": " . $value;
			}
		}
		else
		{
			$headers[] = 'Authorization: ' . S3::__getSignature(
						$this->headers['Host'] == 'cloudfront.amazonaws.com' ? $this->headers['Date'] :
								$this->verb . "\n" . $this->headers['Content-MD5'] . "\n" .
								$this->headers['Content-Type'] . "\n" . $this->headers['Date'] . $amz . "\n" . $this->resource
			);
		}

		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($curl, CURLOPT_WRITEFUNCTION, array(&$this, '__responseWriteCallback'));
		curl_setopt($curl, CURLOPT_HEADERFUNCTION, array(&$this, '__responseHeaderCallback'));
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

		// Request types
		switch ($this->verb)
		{
			case 'GET': break;
			case 'PUT': case 'POST': // POST only used for CloudFront
				if ($this->fp !== false)
				{
					curl_setopt($curl, CURLOPT_PUT, true);
					curl_setopt($curl, CURLOPT_INFILE, $this->fp);
					if ($this->size >= 0)
						curl_setopt($curl, CURLOPT_INFILESIZE, $this->size);
				} elseif ($this->data !== false)
				{
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->verb);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $this->data);
					if ($this->size >= 0)
						curl_setopt($curl, CURLOPT_BUFFERSIZE, $this->size);
				} else
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->verb);
				break;
			case 'HEAD':
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'HEAD');
				curl_setopt($curl, CURLOPT_NOBODY, true);
				break;
			case 'DELETE':
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			default: break;
		}

		// Execute, grab errors
		if (curl_exec($curl))
			$this->response->code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		else
			$this->response->error = array(
				'code' => curl_errno($curl),
				'message' => curl_error($curl),
				'resource' => $this->resource
			);

		@curl_close($curl);

		// Parse body
		if ($this->response->error === false && isset($this->response->headers['type']) && isset($this->response->body))
		{
			if($this->response->headers['type'] == 'application/json')
			{
				$this->response->body = json_decode($this->response->body);
			}elseif($this->response->headers['type']== 'application/xml')
			{
				$this->response->body = simplexml_load_string($this->response->body);
			}
			// Grab S3 errors
			if (!in_array($this->response->code, array(200, 204)) &&
					isset($this->response->body->Code, $this->response->body->Message))
			{
				$this->response->error = array(
					'code' => (string) $this->response->body->Code,
					'message' => (string) $this->response->body->Message
				);
				if (isset($this->response->body->Resource))
					$this->response->error['resource'] = (string) $this->response->body->Resource;
				unset($this->response->body);
			}
		}

		// Clean up file resources
		if ($this->fp !== false && is_resource($this->fp))
			fclose($this->fp);

		return $this->response;
	}

	/**
	 * CURL write callback
	 *
	 * @param resource &$curl CURL resource
	 * @param string &$data Data
	 * @return integer
	 */
	private function __responseWriteCallback(&$curl, &$data)
	{
		if ($this->response->code == 200 && $this->fp !== false)
			return fwrite($this->fp, $data);
		else
			if (isset($this->response->body))
				$this->response->body .= $data;
			else
				$this->response->body = $data;
		return strlen($data);
	}

	/**
	 * CURL header callback
	 *
	 * @param resource &$curl CURL resource
	 * @param string &$data Data
	 * @return integer
	 */
	private function __responseHeaderCallback(&$curl, &$data)
	{
		if (($strlen = strlen($data)) <= 2)
			return $strlen;
		if (substr($data, 0, 4) == 'HTTP')
			$this->response->code = (int) substr($data, 9, 3);
		else
		{
			list($header, $value) = explode(': ', trim($data), 2);
			if ($header == 'Last-Modified')
				$this->response->headers['time'] = strtotime($value);
			elseif ($header == 'Content-Length')
				$this->response->headers['size'] = (int) $value;
			elseif ($header == 'Content-Type')
				$this->response->headers['type'] = $value;
			elseif ($header == 'ETag')
				$this->response->headers['hash'] = $value{0} == '"' ? substr($value, 1, -1) : $value;
			elseif (preg_match('/^x-amz-meta-.*$/', $header))
				$this->response->headers[$header] = is_numeric($value) ? (int) $value : $value;
		}
		return $strlen;
	}

}
