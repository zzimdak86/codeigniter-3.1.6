<?php
/**
 * @file Example.php
 * @brief REST API Sample
 * @author Shinlib Kang (administrator@firedak.com)
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'libraries/REST_Controller.php';


class Example extends REST_Controller
{
	/**
	 * @brief Constructor.
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @brief Index Page for this controller.
	 */
	public function index_get()
	{
		$this->response([
			'status' => false,
			'error' => '잘못된 접근입니다.'
		], REST_Controller::HTTP_NOT_FOUND);
	}
}